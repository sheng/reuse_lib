branches:

core - the most essential interfaces/classes
general - smallest subset that works in both J2SE and Android
android - Android specific features
hazelcast - hazelcast related classes
master - depends only on J2SE